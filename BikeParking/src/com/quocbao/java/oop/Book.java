package com.quocbao.java.oop;

/**
 * has info about book
 * 
 * make getter, setter
 * 
 * @author KIT1_009
 *
 */
public class Book {
	// field have to be private
	// final called constan

	public String ShopName = "TP";
//	public static final String ShopName = "TP"; // instance contain once value, 
	private String title;
	private String writer;

	// request by method
	// permission
	// encapsulation

	// alt+shift+s > generate Getter/Setter
	// Access Modifier :private, public, protected (default)
	// method public, private

	public String getTitle() {
		return title;
	}

	public String getShopName() {
		return ShopName;
	}

	public void setShopName(String shopName) {
		ShopName = shopName;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getWriter() {
		return writer;
	}

	public void setWriter(String writer) {
		this.writer = writer;
	}

}
