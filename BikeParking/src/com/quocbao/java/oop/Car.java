package com.quocbao.java.oop;

public class Car {
	
	private String modelName;
	
	
	// method means can do something
	// without return type, class name ()
	//contructor can parameter(agument) or not paremeter(default constructor)
	public Car(String modelName) {//has argument
		// create constructor not return
		// to init fields when create instance 
		modelName = "Tesla s1";
		System.out.println("created instance of Car");
	}
	public Car() {
		//default not have argument , if want make myself contructor 
		
	}

	public void fowarding() {
		System.out.println("go go");
	}

	public void backwarding() {
		System.out.println("back back");
	}
}
