package com.quocbao.java.oop;

public class Starter {

	public static void main(String[] args) {
		// TODO: program entry point ( start )
		// control program's flow
//		Book book1 = new Book();
//		Book book1; //error 
//		Book book1 = null; //Null pointer access

		// book1 called by instance of Book
		// "instance" it have means be in memory

		// OOP concepts, data's owner allow data's user

		Book book1 = new Book();
		Book book2 = new Book();
		// Starter request to Book
		book1.setTitle("TitleOfBook1");//field of instance 
		book2.setTitle("TitleOfBook2");
//		Book.ShopName ="ClassField"; //only read, 
		book1.setWriter("Jang");
		System.out.println(book1.getTitle());
		System.out.println(book2.getTitle());
		System.out.println(book1.ShopName);//field of Class (Static field)
		book1.ShopName = "change shop name";
		System.out.println(book2.ShopName);
//		System.out.println(book1.getWriter());
//		System.out.println(book1 instanceof Book); 

	}

}
