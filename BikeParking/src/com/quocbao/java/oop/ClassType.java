package com.quocbao.java.oop;

/**
 * define new Type
 * 
 * @author KIT1_009
 *
 */
public class ClassType {

	// member
	// variables called by field
	int code;

	// function called by method
	void todo() {// this called method
		// to run

	}
}
