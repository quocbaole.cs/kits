package com.quocbao.java.oop;

public class MainCar {
	
	public static void main(String[] args) {
		// TODO: program entry point ( start)
//		Car car = new Car(); <-- default constructor by JVM
		Car car = new Car(); //<-- constructor (initialize fields)
		car.fowarding();
		car.backwarding();
		Car car2 = new Car("Tesla s1");
		car2.fowarding();
		car2.backwarding();
	}

}
