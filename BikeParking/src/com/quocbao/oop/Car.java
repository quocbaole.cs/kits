package com.quocbao.oop;

/**
 * 
 * @author KIT1_009
 *Singleton
 */
public class Car {
	private String modelName;
	private static Car instance = null;//font itallic 


	// method mean something
	private Car() {

	}

	public static Car getInstance() {
		//for JDBC, Spring
		
		if (instance == null) {
			instance = new Car();
		}
		return instance;
		// instance has one in memory
		// Singleton pattern

	}

	public void fowarding() {
		System.out.println("go go");
	}

	public void backwarding() {
		System.out.println("back back");
	}
}
