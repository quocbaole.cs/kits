package com.quocbao.oop;

public class Starter {
	public static void main(String[] args) {
		// TODO: program entry point ( start ) parameter == argument
		Car car = Car.getInstance(); // don't call contructor
		Car car2 = Car.getInstance(); // don't call contructor
		
//		Car car= new Car();
//		Car car2= new Car();

		System.out.println(car);
		System.out.println(car2);
	}

}
