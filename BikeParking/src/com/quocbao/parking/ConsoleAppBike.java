package com.quocbao.parking;

import java.util.Scanner;

public class ConsoleAppBike {
	/**
	 * Bike parking station 1. keyboard input ( in, out ...) 2. parking slot : fixed
	 * numbers of max bike 3.price : outTime - inTime
	 * 
	 * @author QuocBao
	 *
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner sc = new Scanner(System.in);// from where input
		// array's type fixed and same all elements
		String[] space = new String[10];// size 10
//"carNumber, inTime" <-- Coma Spareted Values (CSV formmated)
		//String.split(delimeter)
		do {
			System.out.println("cmd>");
			String cmd = sc.nextLine(); // return String
			if ("quit".equals(cmd)) {

				break; // stop loop
			}
			if ("in".equals(cmd)) {// locally defined
				// find empty <- String[] <- NPE (NullPointException)
				int i;
				// search
				for (i = 0; i < space.length; i++) {
					if (space[i] == null) {// then empty
						break;
					}
				} // end loop

				// after
				if (i == space.length) {// not write 10
					System.out.println("full ");
				} else {// i < space.length
					System.out.println("has empty");
					// can do parking
					System.out.println("car number > ");
					String carNumber = sc.nextLine();
					System.out.println(carNumber + " in -->" + i);
					space[i] = carNumber + "," + System.currentTimeMillis();
					; // <--parked in [i], save String

				}
				System.out.println(i); // how to be empty or not empty ?

			}
			if ("out".equals(cmd)) {
				System.out.println("car number > ");
				String carNumber = sc.nextLine();
				int i;
				// searching
				for (i = 0; i < space.length; i++) {
					if (space[i] != null) {
						String[] car = space[i].split(",");
						if (carNumber.equals(car[0])) {
							// to do when seached car
							System.out.println(carNumber + " out<-- " + i);
							long outTime = System.currentTimeMillis();
							long inTime = Long.parseLong(car[1]);
							long price = (outTime - inTime) * 1000;
							System.out.println("price : " + price);
							space[i] = null; // <--is empty
							break;// or continue
						}
					}
				}

			}

		} while (true); // infinite loop

		sc.close();
		System.out.println("bye ...");
	}// end main ( program out )

	public static void main01(String[] args) {
		// TODO Auto-generated method stub

		Scanner sc = new Scanner(System.in);// from where input
		// array's type fixed and same all elements
		String[] space = new String[10];// size 10
//		int seq = 0; // form 0, +1 when park bike
		long timetable[] = new long[10];
		do {
			System.out.println("cmd>");
			String cmd = sc.nextLine(); // return String
			if ("quit".equals(cmd)) {

				break; // stop loop
			}
			if ("in".equals(cmd)) {// locally defined
				// find empty <- String[] <- NPE (NullPointException)
				int i;
				// search
				for (i = 0; i < space.length; i++) {
					if (space[i] == null) {// then empty
						break;
					}
				} // end loop

				// after
				if (i == space.length) {// not write 10
					System.out.println("full ");
				} else {// i < space.length
					System.out.println("has empty");
					// can do parking
					System.out.println("car number > ");
					String carNumber = sc.nextLine();
					System.out.println(carNumber + " in -->" + i);
					space[i] = carNumber; // <--parked in [i]
					timetable[i] = System.currentTimeMillis();

				}
				System.out.println(i); // how to be empty or not empty ?

			}
			if ("out".equals(cmd)) {
				System.out.println("car number > ");
				String carNumber = sc.nextLine();
				int i;
				// searching
				for (i = 0; i < space.length; i++) {
					if (carNumber.equals(space[i])) {
						// to do when seached car
						System.out.println(carNumber + " out<-- " + i);
						long outTime = System.currentTimeMillis();
						long inTime = timetable[i];
						long price = (outTime - inTime) * 1000;
						System.out.println("price : " + price);
						space[i] = null; // <--is empty
						break;// or continue
					}
				}

			}

		} while (true); // infinite loop

		sc.close();
		System.out.println("bye ...");
	}// end main ( program out )

}
