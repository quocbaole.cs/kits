package com.quocbao.parking;

public class UseHeap {
	
	public static void main(String[] args) {

		// Reference type : Class's object(instance)
		// 1. null means have no in memory
		// 2. void mean have no return value;
		String jang = "Jang"; //(once )these same it create only once 
		String jang1 = "Jang";//is just once 
		System.out.println(jang + "@"+((Object)jang).hashCode());//convert String -> Object
		System.out.println(jang1 + "@"+jang.hashCode());
	//java.lang.Object.hashCode() !=  java.lang.String.hashCode()(caculate)

		String[] names = new String[5];
		addName(names);
		addName(names);
		addName(names);
		System.out.println(names[0].hashCode());
		
	
		//Object's say hashCode is memory address
		//with String's hashCode is not memory address
		
	}

	private static void addName(String[] names) {
		// TODO Auto-generated method stub
//		names[0] = "Bao";
		names[0] = new String("Jang");
	}

	private static void todo(String[] names) {
		// TODO Auto-generated method stub
		names = new String[3]; // in Heap
		// VM's GC (Virtual Machine Garbage Collector)
		System.out.println(names);
		// remember it, locally new array
		names = null; // to safe memory, unused array remove, then remove it
		// efficiently use memory

	}

}
