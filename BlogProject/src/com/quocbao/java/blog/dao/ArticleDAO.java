package com.quocbao.java.blog.dao;

import com.quocbao.java.blog.Article;

//method's rule like design for project
//data can CRUD
public interface ArticleDAO {
	// what's data type?
	public void insert(Article vo);

	public void update(Article vo);

	public void delete(int id);

	public Article[] select();// all articles

	public Article[] selectBy(Article vo);

	public Article[] selectByWriter(String writer);

	public Article selectOne(String key);
	public Article[] getAll();
}
