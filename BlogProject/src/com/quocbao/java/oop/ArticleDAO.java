package com.quocbao.java.oop;

/**
 * Model role DAO : Data Access Object impletment CRUD for Article
 * 
 * @author KIT1_009
 *
 */
public class ArticleDAO {
	private static Article[] list;
	private static int seq;

	// default constructor
	public ArticleDAO() {
		list = new Article[10]; // length fixed 10
	}

	public boolean save(Article article) {
		boolean result = false;
		// how to add, index ++
		if (seq < list.length) {
			list[seq] = article;
			++seq;
			result = true;
		}
		return result;

	}

	public Article getArticle(int idx) {
		// is Exist ?
		Article origin = list[idx];// get reference
		if (origin != null) {
			Article rt = new Article(origin);
			return rt;// copy instance
		}
		return null;
	}

	public void update(Article article, int idx) {
		list[idx] = article;
	}

	public Article[] searchArticleByWriter(String writer) {
		// 1. count article which has writer
		int count = 0;
		for (int i = 0; i < list.length; i++) {
			// use Reference type Array
			if (list[i] != null) {
				if (list[i].getWriterName().equals(writer)) {
					count += 1;
				}
			}
		}
		if (count > 0) {
			// has writer
			Article[] result = new Article[count];
			int k = 0; // index of result
			for (int i = 0; i < list.length; i++) {
				// use Reference type Array
				if (list[i] != null) {
					if (list[i].getWriterName().equals(writer)) {
						Article rt = new Article(list[i]);
//						rt.setSeq(list[i].getSeq());
//						rt.setTitle(list[i].getTitle());
//						rt.setContent(list[i].getContent());
//						rt.setWriterName(list[i].getWriterName());
						result[k] = rt;
						++k;
					}
				}
			}
			return result;
		}
		return null;
	}

	public Article[] getAll() {
		// private field "list"
		// can't be change vy other class
		//broken means 
		System.out.println(list+"in dao");
		return list.clone();
	}

}
