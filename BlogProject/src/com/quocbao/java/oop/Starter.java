package com.quocbao.java.oop;

import java.util.Scanner;

/**
 * @Author QuocBao
 */

//Control role
public class Starter {
	static Scanner sc;
//	static Article[] list; //<--model is data crud 
	static int temp;
	static ArticleDAO dao;

	public static void main(String[] args) {
		// TODO: program entry point ( start )
		sc = new Scanner(System.in);
		dao = new ArticleDAO();
		// list = new Article[10];

		while (true) {
			// show all article list
			// if get reference , can be change

			Article[] list = dao.getAll();

//			System.out.println(list + "in main");
			// String view = showList(dao.getAll());
			// System.out.println(view);
			if (list[0] != null) {
				list[0] = new Article();
				list[0].setTitle("in man main");
			}
			// request from keyboard
			System.out.println(">");
			String cmd = sc.nextLine();
			String delimiters = "\\s|/";
			String[] str = cmd.split(delimiters);
			// control for request
			if (str[0].equals("new")) {// Create
				registArticle();
			}

			if (str[0].equals("edit")) {// Update
				if (isNumberic(str[1])) {
					int idx = Integer.parseInt(str[1]);// 1
					editArticle(idx); // go to edit [idx]article
				} else {
					System.out.println("invalid");
				}

			}
			if (str[0].equals("search")) {
				if (!isNumberic(str[1])) {
					Article[] rs = dao.searchArticleByWriter(str[1]);
					// writer can write many articles
					if (rs != null) {
						String s = showList(rs);
						System.out.println(s);
					}
				} else {
					System.out.println("invalid");
				}
			}

		} // end while
	}

	private static String showList(Article[] rs) {
//		rendering by View role
		String s = "Search result\n";
		for (int i = 0; i < rs.length; i++) {
			if (rs[i] != null) {
				s += rs[i].getTitle();
				s += "(" + rs[i].getWriterName() + ")";
			}
		}
		s += "----------------------";

		return s;
	}

	private static void editArticle(int idx) {
		Article article = dao.getArticle(idx);
		if (article == null) {// have to check null , NPE
			System.out.println("incalid index");
			return;
		}
		// Article article = list[idx];// get reference
		System.out.println("Title > " + article.getTitle());
		String title = sc.nextLine();
		// press only <enter> , empty string
		if (title.length() > 0) { // have input data
			article.setTitle(title);// over write data
		}
		System.out.println("Writer > " + article.getWriterName());
		String name = sc.nextLine();
		if (!name.isEmpty()) { // have input data
			article.setWriterName(name);// over write data
		}
		System.out.println("Content > " + article.getContent());
		String content = sc.nextLine();
		if (content.length() > 0) { // have input data
			article.setContent(content);
		}
		dao.update(article, idx);
		// quiz ? tai sao khong co code below
		// list[idx] = article; already set via setter

	}

	private static void registArticle() {
		// data read from keyboard
		System.out.println("Title > ");
		String title = sc.nextLine();
		System.out.println("Writer > ");
		String name = sc.nextLine();
		System.out.println("Content > ");
		String content = sc.nextLine();
		// save data in Article instance
		Article article = new Article();
		article.setTitle(title);
		article.setWriterName(name);
		article.setContent(content);
		// save to array in DAO
		// delegate to dao <-- call method of dao
		if (!dao.save(article)) {
			System.out.println("failed");
		}

	}

	private static boolean isNumberic(String str) {
		if (str == null) {
			return false;
		} else {
			try {
				double number = Double.parseDouble(str);

			} catch (NumberFormatException e) {
				// TODO: handle exception
				return false;
			}
			return true;
		}
	}

}
