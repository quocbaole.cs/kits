package com.quocbao.blog;

import java.util.Scanner;

public class Main {
	static Scanner sc;
	static ArticleDAO dao;

	public static void main(String[] args) {

		sc = new Scanner(System.in);
		dao = new ArticaleDAOIpml();
		String determine = "\\s|/";
		while (true) {
			System.out.println("cmd >");
			String cmd = sc.nextLine();
			String[] str = cmd.split(determine);
			Article[] list = dao.select();
			System.out.println(list.length);
			System.out.println("in main" + list.hashCode());

			if (str[0].equals("new")) {
				regisArticle();
			}
			if (str[0].equals("update")) {
//				cmd = sc.nextLine();
				int idx = Integer.parseInt(str[1]);
				updateArticle(idx);
				System.out.println("updated");
			}
			if (str[0].equals("search")) {
				System.out.println("searched");
				//System.out.println("select name writer > ");
				//cmd = sc.nextLine();
				if (isSearch(cmd)) {
					Article[] sr = dao.selectByWriter(str[1]);
					System.out.println(sr.length);
					for (int i = 0; i < sr.length; i++) {
						System.out.println(list[i].getTitle() + "(" + list[i].getWriterName() + ")\n");
					}
				} else {
					System.out.println("invalid");
				}

			}
			if (str[0].equals("delete")) {
				//cmd = sc.nextLine();
				int idx = Integer.parseInt(str[1]);
				dao.deleteByIdx(idx);
			}
			if (str[0].equals("read")) {
				for (int i = 0; i < list.length; i++) {
					System.out.println(list[i].getTitle() + "(" + list[i].getWriterName() + ")");
				}
			}
		}
	}

	private static void regisArticle() {
		System.out.println("title > ");
		String title = sc.nextLine();
		System.out.println("writer >");
		String writer = sc.nextLine();
		System.out.println("content >");
		String content = sc.nextLine();
		Article article = new Article(title, writer, content);
		dao.insert(article);
	}

	private static void updateArticle(int idx) {
		Article article = new Article();
		System.out.println("title >");
		String title = sc.nextLine();
		if (title.length() > 0) {
			article.setTitle(title);
		}
		System.out.println("writer > ");
		String writer = sc.nextLine();
		if (writer.length() > 0) {
			article.setWriterName(writer);
		}
		System.out.println("content > ");
		String content = sc.nextLine();
		if (content.length() > 0) {
			article.setContent(content);
		}
		dao.update(idx, article);
	}

	private static boolean isSearch(String writer) {
		if (dao.selectByWriter(writer) == null) {
			return false;
		}
		return true;
	}

}
