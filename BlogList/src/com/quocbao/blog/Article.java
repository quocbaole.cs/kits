package com.quocbao.blog;

public class Article {
	// field
	private long seq;
	private String title;
	private String writerName;
	private String content;

	public Article() {

	}

	// make clone method as Constructor
	// Copy Constructor --> don't default
	public Article(Article arg) {
		// this mean new instance
		this.seq = arg.getSeq();
		this.title = arg.getTitle();
		this.writerName = arg.getWriterName();
		this.content = arg.getContent();
	}

	public Article copyTo(Article arg) {
		// this mean new instance
		Article article = new Article();

		article.setSeq(arg.getSeq());
		article.setTitle(arg.getTitle());
		article.setContent(arg.getContent());
		article.setWriterName(arg.getWriterName());
		return article;
	}

	// has all argument for fields
	// constructor using Fields
	public Article(long seq, String title, String writerName, String content) {
		super();
		this.seq = seq;
		this.title = title;
		this.writerName = writerName;
		this.content = content;
	}

	public Article(String title, String writerName, String content) {
		super();

		this.title = title;
		this.writerName = writerName;
		this.content = content;
	}

	public long getSeq() {
		return seq;
	}

	public void setSeq(long seq) {
		this.seq = seq;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getWriterName() {
		return writerName;
	}

	public void setWriterName(String writerName) {
		this.writerName = writerName;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
