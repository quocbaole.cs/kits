package com.quocbao.blog;



public interface ArticleDAO {

	void deleteByIdx(int idx);

	void update(int idx,Article article);

	void insert(Article article);

	public Article[] select(); //get all articale

	Article[] selectByWriter(String writer);



}
