package com.quocbao.blog;

import java.util.ArrayList;

public class ArticaleDAOIpml implements ArticleDAO {

	private static ArrayList<Article> list;

	public ArticaleDAOIpml() {
		list = new ArrayList<Article>();
	}

	@Override
	public void insert(Article article) {

		Article ar = new Article(article);
		list.add(ar);

	}

	@Override
	public Article[] select() {
		Article[] article = new Article[list.size()];
		System.out.println("in dao: " + article.hashCode());
		for (int i = 0; i < list.size(); i++) { // get all : convert arraylist -> list
			article[i] = list.get(i);
		}
		return article.clone();
	}

	@Override
	public Article[] selectByWriter(String writer) {
		ArrayList<Article> arr = new ArrayList<Article>();
		for (int i = 0; i < list.size(); i++) {
			if (writer.equals(list.get(i).getWriterName())) {
				arr.add(list.get(i));
			}
		}
		Article[] article = new Article[arr.size()];
		for (int i = 0; i < arr.size(); i++) { // get all : convert arraylist -> list
			article[i] = arr.get(i);
		}
		return article.clone();
	}

	@Override
	public void update(int idx, Article article) {
		Article arr = new Article(article);
		list.set(idx, arr);
	}

	@Override
	public void deleteByIdx(int idx) {
		// TODO Auto-generated method stub
		if (idx < list.size()) {
			list.remove(idx);
		} else {
			System.out.println("invalid");
		}
	}

}
