import java.util.Random;

public class Output {

	static int count = 0;
	
	public static void main(String[] args) {
		printNumber();
		//loop();
		//printNumber02(1, 27, 5);
		//printNumber03(1, 27, 5);
		//generateRandom02();
		
//		int r = (int)(Math.random()*1000);
//		if(check01(r)) {
//			System.out.print("Exactly");
//		}
//		else {
//			
//		}
		
		//quiz01();
		
		//System.out.println(hasDuplicate(375));//check password
		//System.out.println(isDuplicate(1234));\
		/*
		bit-wise operator;
		1. masking : check Bit >> <<
		 */
		//procBitwise01();
		//procBitwise02();
		//procBitwise07();
		
	}
	
	static void procBitwise07() {
		int n = 25;
		String bin = Integer.toBinaryString(n);
		System.out.println(bin);
		
		int clearBit = 1<<3;
		
		n = n & ~clearBit;
		
		bin = Integer.toBinaryString(n);
		System.out.println(bin);
		
	}
	
	static void procBitwise06() {
		int n = 25;
		String bin = Integer.toBinaryString(n);
		System.out.println(bin);
		
		int setBit = 1<<2;
		
		n = n | setBit;
		
		bin = Integer.toBinaryString(n);
		System.out.println(bin);
	}
	
	static void procBitwise05() {
		int n = 25;
		String bin = Integer.toBinaryString(n);
		System.out.println(bin);
		
		int checkBit = 1<<3 ;//masking is check bit value
		System.out.println(Integer.toBinaryString(checkBit));
		
		int xor = n^(1<<3);
		System.out.println(Integer.toBinaryString(xor));
		n = xor^(1<<3);
		
		System.out.println(Integer.toBinaryString(n));
	}
	
	static void procBitwise04() {
		int n = 24;
		// how 6 bit value of n
		String bin = Integer.toBinaryString(n);
		System.out.println(bin);
		
		int checkBit = 1<<6 ;//masking is check bit value
		System.out.println(Integer.toBinaryString(checkBit));
		
		int result = n & checkBit;
		
		if(result>0) {
			System.out.println("[6]bit's value is 1");
		}else {
			System.out.println("[6]bit's value is 0");
		}
	}
	
	static void procBitwise03() {
		//shift operator
		int n = 0b10000001;
		//left shift << x times move to high
		//right shift >> x times move to low
		String bin = Integer.toBinaryString(n>>1);
		System.out.println(bin);
	}
	
	static void procBitwise02() {
		int n = 0b1001;
		int z = 0b1111;
		//int rs = n & z;// AND 0->0
		//int rs = n | z;// OR 1->1
		int rs = n ^ z;// XOR not equal -> 1
		String bin = Integer.toBinaryString(rs);
		System.out.println(bin);
	}
	
	static void procBitwise01() {
		int n=13;// if -13 => 32bits
		System.out.println(~n+1); // 13DEC
		System.out.println(Integer.toBinaryString(n)); // binary(BIN)
		System.out.println(Integer.toBinaryString(~n));
	}
	
	static boolean isDuplicate(int arg) {
		boolean rs = false;
		int temp=arg;
		int flag=0;
		while(temp>0) {
			int d = temp%10;
			if((flag & (1<<d))>0) {
				rs=true;
			}
			else {
				flag = flag | (1<<d);  
			}
			temp=temp/10;
		}
		return rs;
	}
	
	static boolean hasDuplicate(int arg) {
		boolean rs = false;
		int a = arg/100;
		int b = (arg/10)%10;
		int c = arg%10;
		rs = (a==b) || (a==c) || (b==c);
		return rs;
	}
	
	static void quiz01() {
		Random gen = new Random();
		int min=gen.nextInt(100);
		int max=gen.nextInt(100);
		
		if(max<min) {
			int t=min;
			min=max;
			max=t;
		}	
		
		int count=0;
		for(int i=min;i<=max;i++) {
			System.out.print(i+"\t");
			count++;
			if(count==6) {
				System.out.println();
				count=0;
			}
		}
		
		System.out.println("\nmin: "+min);
		System.out.println("max: "+max);
	}
	
	static boolean check01(int parameter) {
		if(parameter>=100 && parameter <=999) {
			return true;
		}
		return false;
	}
	
	static void generateRandom01() {
		//Math.random();// data type: double (0 ~ 1)
		System.out.println(Math.random());
		System.out.println(Math.random()*100000000);
		System.out.println((int)(Math.random()*100000000));// type casting double --> int
	}
	
	static void generateRandom02() {
		Random gen = new Random();
		System.out.println(gen.nextInt());
		System.out.println(gen.nextBoolean());
		System.out.println(gen.nextFloat());
		System.out.println(gen.nextDouble());
		System.out.println(gen.nextLong());
		
		System.out.println(gen.nextInt(10)); // <10
		int min=10;
		int max=30;
		System.out.println(gen.nextInt(max+1));
	}
	
	static void loop() {
	    count++;
	    if (count <= 27) {
	        System.out.print(count+" ");
	        loop();
	    }
	    System.out.print("\n");
	}
	
	static void printNumber() {
		/*
		 * int j=1; while(j<=27) { System.out.print(j+" "); j++; }
		 */
		
		for(int i=1;i<=27;i+=1) {
			System.out.print(i+" ");
		}
		System.out.print("\n");
	}
	
	static void printNumber02(int start,int end,int limit) {
		//int count=0;
		for(int v=start;v<=end;v+=1) {
			System.out.print(v+" ");
			count++;
			if(v%limit==0) {
				System.out.print("\n");
				//count=0;
			}
		}
	}
	
	static void printNumber03(int start,int end,int limit) {
		int value=start;
		
		int lines =(end-start+1)/limit;//5
		lines += ((end-start+1)%limit>0)?1:0;//5+1
		
		for(int row=0;row<lines;row+=1) {
			for(int col=0;col<limit;col+=1) {
				System.out.print(value+" ");
				if(value==end) {
					break;
				}
				value++;	
			}
			System.out.println();
		}
		
		/*
		 * for(int i=1;i<=9;i++) { for(int j=2;j<=9;j++) {
		 * System.out.print(j+"x"+i+"="+i*j+" "); } System.out.println(); }
		 */
	}

}
