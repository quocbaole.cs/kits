package Quiz02;

import java.io.IOException;

public class Quiz02_NguyenHuy_08_VietMinh_09 {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		quiz02();
	}
	
	private static void quiz02() throws IOException {
		int keyValue;
		int total=0;
		int flag=0;
		int temp=0;
		int num=0;
		while(true) {
			keyValue = System.in.read();		
			if(keyValue==-1) {
				break;
			}
			if(keyValue>='0' && keyValue<='9') {
				flag=1;
				num=keyValue-48;
				temp=temp*10+num;
			}
			else {
				if(flag==1) {
					total=total+temp;
					temp=0;
				}
				flag=0;
			}
		}
		System.out.println(total);
	}
}