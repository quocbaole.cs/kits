package Tutorial;

import java.io.IOException;
import java.util.Random;

public class Tutorial02 {

	public static void main(String[] args) throws IOException{
		/*
		 Hay nhap random tu ban phim vao sau do xuat tong cac so nguyen
		 */
		quiz02();
		
	}
	
	private static void quiz02() throws IOException{
		int keyValue;
		int total=0;
		boolean isDigit=false;
		int num=0;// each integer value
		while(true) {
			keyValue = System.in.read();		
			if(keyValue==-1) {//Ctrl+z
				break;
			}
			if(keyValue >= '0' && keyValue <= '9') {
				isDigit=true;
				//'1':49 -> 1
				num=num*10+(keyValue-48);                                                                                                                                 
			}else {
				if(isDigit==true) {
					total=total+num;
					num=0;// reset next int
				}
				isDigit=false;			
			}
		}
		System.out.println(total);
	}
		
}
	
	
