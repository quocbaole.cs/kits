package Tutorial03;

import java.util.Scanner;

public class Tutorial03 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		updown();
	}
	// binary search
	private static void updown() {
		Scanner sc = new Scanner(System.in);
		int min = 1;
		int max = 100;
		System.out.println("fix your number(1~100): ");
		System.out.print("> ");
		int call=0;
		sc.nextLine();              
		while(true) {
			call = (min+max) / 2;
			System.out.println("Your number "+call+"?");
			System.out.print("> ");
			String str = sc.nextLine();
			if(str.equals("0")) {
				break;
			}
			if(str.equals("up")) {
				// answer dont have call -> max
				max=call;
			}
			if(str.equals("down")) {
				min=call;
			}
			System.out.println(str);
		}
		sc.close();
	}

}
