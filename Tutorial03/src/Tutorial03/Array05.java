package Tutorial03;

import java.util.Scanner;

public class Array05 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//parking station
		int[] station = new int[5];// bike's number
		Scanner sc = new Scanner(System.in);
		
		while(true) {
			for(int i=0; i<station.length; i++) {
				System.out.print(station[i]+"\t");
			}
			
			System.out.println("\ncmd >");
			String cmd = sc.nextLine();
			if(cmd.equals("quit")) {
				break;
			}
			
			if(cmd.equals("in")) {
				int idx = -1;
				
				for(int i=0; i<station.length; i++) {
					if(station[i] == 0) {
						idx = i;
						break;
					}
				}
				
				if(idx == -1) {
					System.out.println("Full station");
				}else {
					// start park bike
					System.out.println("Parking bike ? ");
					cmd = sc.nextLine();
					int bikeNumber = Integer.parseInt(cmd);
					station[idx] = bikeNumber; //parked this
				}
			}
			
			if(cmd.equals("out")) {
				System.out.println("Outgoing bike ? ");
				cmd = sc.nextLine();
				int bikeNumber = Integer.parseInt(cmd);
				int idx = -1;
				
				for(int i=0; i<station.length; i++) {
					if(station[i] == bikeNumber) {
						idx = i;
						break;
					}
				}
				
				if(idx != -1) {
					station[idx] = 0;
				}else {
					System.out.println("No bike");
				}
			}
		}
		sc.close();
		System.out.println("END");
	}

}
