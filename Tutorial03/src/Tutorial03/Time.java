package Tutorial03;

//java 1.8 over
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

//java 1.7 under
import java.util.Calendar;
import java.util.Date;

public class Time {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		long milli_seconds = System.currentTimeMillis();
		System.out.println(milli_seconds);
		System.out.println(milli_seconds/1000);
		System.out.println(milli_seconds/1000/60);
		//parking(args);
		
		Date dnow = new Date();
		dnow.getTime();
		System.out.println(dnow.getHours());//deprecated (no use) (according java version)
		
		Calendar cal = Calendar.getInstance();
		System.out.println(cal.get(Calendar.HOUR_OF_DAY));
		
		// Java 18.8 over
		LocalDate ldt1 = LocalDate.now();
		LocalTime ldt2 = LocalTime.now();
		LocalDateTime ldt3 = LocalDateTime.now();
		
		System.out.println(ldt1);
		System.out.println(ldt2.getMinute());
		System.out.println(ldt3);
	}

}
