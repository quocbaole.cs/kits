package Tutorial03;

public class Array03 {

	/*
	 * CRUD:
	 * Retrieve: showList()
	 * Update: update()
	 * Delete: delete()
	 * */
	
	static int[] list;
	static int seq = 0;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		list = new int[10];
		insert(4);
		insert(5);
		insert(3);
		insert(56);
		showList();
		update(5, 100);//update(index, value)
		System.out.println();
		delete(1);
		insert(3);
		showList();
	}
	
	private static void delete(int delIndex) {
		for(int i=delIndex; i<seq-1; i++) {
			list[i]=list[i+1];
		}
		seq--;
	}
	
	private static void update(int idx, int value) {
		if(idx < seq) {
			list[idx] = value;
		}else {
			System.out.println("Invalid index!");
		}
	}
	
	private static void showList() {
		for(int i=0; i<seq; i++) {
			System.out.println("["+i+"] "+list[i]+" ");
		}
	}
	
	private static void insert(int arg) {
		// local variable be reset
		if(list==null) {// NPException : NullPointerException
			System.out.println("Cant insert!");
			return;
		}
		if( seq < list.length ) {
			list[seq] = arg;
			seq+=1;
		}else {
			System.out.println("List full!");
		}
	}

}
