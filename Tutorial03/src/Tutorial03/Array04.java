package Tutorial03;

public class Array04 {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] score = {10,60,21,40,70};
		showList(score);
		int[] result = lessthan(score, 50);// return int[]
		showList(result);
		
		System.out.println(score);
		System.out.println(result);
	}
	
	private static int[] lessthan(int[] param, int lt) {
		int size=0;
		
		for(int i=0; i<param.length; i++) {
			if(param[i]<lt) {
				size+=1;
			}
		}
		
		int[] result = new int[size];
		int k=0;
		for(int i=0; i<param.length; i++) {
			if(param[i]<lt) {
				result[k] = param[i];
				k++;
			}
		}
		return result;
	}
	
	private static void showList(int[] score) {
		String str = "[";
		for(int i=0; i<score.length; i++) {
			str += score[i];
			if(i<score.length-1){
				str += ", ";
			}
		}
		str += "]";
		System.out.println(str);
	}
}
