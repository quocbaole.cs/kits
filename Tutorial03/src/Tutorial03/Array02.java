package Tutorial03;

public class Array02 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//selection sort
		int[] list = {91,34,2,14,25,8,21};
		//int[] copy = new int[list.length];
		int[] copy = list; // gan = refernce 
		//System.arraycopy(list, 0, copy, 0, copy.length);// copy array change adrress(reference)
		
		System.out.println(list);
		System.out.println(copy);
		
		list[0] = 100; // change to copy
	
		System.out.println();
		for(int i=0; i<copy.length; i++) {
			System.out.print(copy[i]+" ");// copy = l ist
		}
		
		for(int i=0; i<list.length; i++) {
			int max = i;
			for(int j=i+1; j<list.length; j++) {
				if(list[max]<list[j]) {
					max=j;
				}
			}
			int temp = list[i];
			list[i] = list[max];
			list[max] = temp;
		}
		System.out.println();
		for(int i=0; i<list.length; i++) {
			System.out.print(list[i]+" ");
		}
	}

}
