package com.quocbao;

public class Array01 {

	public static void main(String[] args) {
		int arr[] = new int[10];
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + "\t");
		}
		System.out.println("/n");
		int[] score = new int[10];
		int last = -1;
		for (int i = 0; i < score.length; i++) {
			if (last == 7) {
				break;
			}
			last++;

			score[i] = i;
		}
		for (int i = last; i >= 0; i--) {
			System.out.print(score[i] + " ");
		}

		int[] list = { 1, 24, 20, 34, 25, 10 };
		int max_value = list[0];
		int min_value = list[0];
		int max_index = 0;
		int min_index = 0;
		for (int i = 1; i < list.length; i++) {
			if (list[i] >= max_value) {
				max_index = i;
				max_value = list[i];

			}
			if (list[i] <= min_value) {
				min_index = i;
				min_value = list[i];
			}
		}
		System.out.println("\n");
		System.out.println("[" + max_index + "] " + max_value);
		System.out.println("[" + min_index + "] " + min_value);
	}
}
