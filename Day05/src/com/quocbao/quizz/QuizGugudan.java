package com.quocbao.quizz;

public class QuizGugudan {
	static int temp = 0;

	public static void main(String[] args) {
		// Danh gia cap do ca nhan
		// in bang cuu chuong
		gugudan(2);// 2 ~ 9

	}

	private static void gugudan(int cols) {
		// what change, how change , has pattern <---- programming sense

		for (int base = 2; base < 10; base += cols) {
			for (int v = 1; v < 10; v++) {
				for (int c = 0; c < cols; c++) {
					int dan = base + c;
					int multi = dan + v;
					if (dan < 10) {
						System.out.print(dan + "x" + v + "=" + multi + "\t");
					}
				}
				System.out.println();
			}
		}
	}

	private static void gugudan2(int cols) {
		for (int v = 1; v < 10; v++) {
			int dan = 2;
			for (int i = 0; i < cols; i += 1) {
				if (dan < 10) {
					int mul = dan * v;
					System.out.print(dan + " x " + v + " =" + mul + "\t");

				}
				++dan; // <- 2 <-3 <-4
			}
			System.out.println("");

		}
	}

	private static void gugudan1(int cols) {

		for (int bias = 0; bias < 10; bias++) {
			int dan = 2;
			for (int i = 0; i < cols; i += 1) {
				if (dan < 10) {
					int mul = dan * 1;
					System.out.print(dan + " x 1 =" + mul + "\t");

				}
				++dan; // <- 2 <-3 <-4
			}
			System.out.println("");

		}
	}

}
