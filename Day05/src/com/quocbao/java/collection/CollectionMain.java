package com.quocbao.java.collection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class CollectionMain {
	/**
	 * Colleciton framework : Library of Java 
	 * -data structure algorithm -data
	 * -structrue is many data save way 
	 * -use Generic to select actually use save type 
	 * -can change data type when create instance 
	 * -dynamic structure 
	 * +can changed length (numbers of element) 
	 * + add(+), remove (-), but array[] fixed length
	 */
	public static void main(String[] args) {
		//set();// File, DB
		
		map(); //include Set 
	}
	/**
	 * Key-Value : like variables name: value
	 * point!!
	 *  variable's name cant be change in running
	 *  k-v can be change and add in running
	 */
	
	private static void map() {
		//Map<K, V> is interface 
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("a", 10);
		map.put("bao", 100);
		
		//get by Key(name)
		System.out.println(map.get("a"));
		System.out.println(map.get("bao"));
		System.out.println("loop");
		//loop
		// 1. what need to get value in value
		System.out.println("method 1--------");
		Set<String> keys = map.keySet();
		for (String key : keys) {
			System.out.println(map.get(key));
		}
		System.out.println("method 2: ");
		map.forEach((k,v)->System.out.println("key: "+k+":"+v));
		
		//System is running but map can addition variable 
//		while(true) {
//			map.put("a", 10); //we can define dynamically (add)
//			//a = 10; Oh~awesome
//			
//		}
		
		
	}

	private static void set() {
//		Set<Integer> set = null;//NullPointerException (NPE) 
		TreeSet<Integer> set = new TreeSet<Integer>();
		//sorted by tree structure 
		//want get according value
		set.add(10);
		set.add(20);
		set.add(30);
		set.add(2);
		System.out.println(set.first());// min  
		System.out.println(set.last());// max
		
		if(set!=null) {
			for (Integer el : set) {
				System.out.println(el);
			}
		}
		
	}

	public static void main02(String[] args) {
		hashset();
		int hv = System.identityHashCode("william");
		System.out.println(hv);
		//String s= new String("william"); ref is different 
		String s1= "william";
		hv = System.identityHashCode(s1);
		System.out.println(hv);
	}
/**
 * has value: value to (identified)
 * - (jang) -> 2131236
 * -has value is different, then data is different
 * -- data is data is same, has value is same;
 */
	private static void hashset() {
		// Set<E> is interface
		Set<String> set = new HashSet<String>();// Object.hasCode();, use: File, DB
		// add
		set.add("jang"); //ex: hash value: 123
		set.add("mario");//ex: 345
		set.add(new String("william"));//hash value: 567
		set.add("william");//hash value: 567
		// print all elements
		// 1. return element by index(i)
		// non sequencial when add, so without index 
		// 2. return numbers of element
		System.out.println(set);
//		for(int i =0; i < set.size(); i++) {
//			
//		}
//		Method 1
		System.out.println("Method 1----------");
		for (String item : set) {
			System.out.println(item);
		}

		System.out.println("Method 2----------");
		//Method 2
		Iterator<String> itr = set.iterator();
		while (itr.hasNext()) {
			System.out.println(itr.next());
		}
		
		System.out.println("Method 3--------");
		//Method 3
		for(Iterator<String> it = set.iterator(); it.hasNext();) {
			String item = it.next();
			System.out.println(item);
		}
		System.out.println("Method 4---------------");
		//Method 4
		set.forEach(item->System.out.println(item));
		System.out.println("Method 5----------");
		set.forEach(System.out::println);
	
	}

	public static void main01(String[] args) {

		// List<E> is interface has Generic <element type>
		// List<> is parent of ArrayList, LinkedList
		List<Integer> alist = new ArrayList<Integer>();// diamond operator
		// Integer is wrapper class for int
		List<String> llist = new LinkedList<String>();

		// usage same
		alist.add(10);
		alist.add(20);
		llist.add("jang");
		System.out.println(alist.size());// current numbers
		System.out.println(llist.size());// but, array.length max numbers
		// loop: for(), for-each(), while()
		for (int i = 0; i < alist.size(); i++) {
			System.out.println(alist.get(i));
		}
		for (int i = 0; i < llist.size(); i++) {
			System.out.println(llist.get(i));
		}
	}

}
