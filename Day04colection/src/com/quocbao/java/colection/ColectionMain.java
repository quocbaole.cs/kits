package com.quocbao.java.colection;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class ColectionMain {

	public static void main(String[] args) {
		arrayListString();
	}

	public static void removeItem() {
		ArrayList<String> list = new ArrayList<String>();// arrayList type
		list.add("toy");
		list.add("apple");
		list.add("banana");
		list.add("banana");
		System.out.println(list.size());
		for (String item : list) {
//			[0]toy[1]apple[2]banana <- [3] empty so stop
			if (item.equals("banana")) {
				list.remove(item);
			}
		}
		System.out.println(list.size());
		for (String string : list) {
			System.out.println(string);
		}
	}

	public static void arrayListString() {
		ArrayList<String> list = new ArrayList<String>();// arrayList type
		list.add("toy");
		list.add("apple");
		list.add("banana");
		list.add("banana");
		// Method 1;
		Iterator<String> itr = list.iterator();
		while (itr.hasNext()) {// hasNext has element to get ?
			String el = itr.next();
			System.out.println(el);
		}
		System.out.println("----------------");
		// Method 2:
		for (String item : list) {// for-each need has Iterator
			System.out.println(item);
		}

		// Remove
		String del = null;
		for (String el : list) {
//			System.out.println(el);
			if (el.equals("banana")) {
				del = el;
				break;
			}
		}
		// after search
		if (del != null) {// recommended
			list.remove(del);
		}
		// after remove
		System.out.println("after search and after remove : ");
		for (String item : list) {// for-each need has Iterator
			System.out.println(item);
		}
		System.out.println("add all----------------");
		// add All
		ArrayList<String> list2 = new ArrayList<String>();
		list2.add("apple 1");
		list2.add("oppo");
		list2.add("samsung");
		list2.addAll(list);

		Iterator<String> it = list2.iterator();
		while (it.hasNext()) {
			System.out.println(it.next());
		}
		System.out.println("--------");
		// oppo have in list2
		System.out.println(list2.contains("oppo"));
		System.out.println(list2.lastIndexOf("oppo"));
	}

	public static void likedList() {
		List<Integer> list = new LinkedList<Integer>();
		int i;
		for (i = 1; i < 10; i++) {
			list.add(i);
		}
//		for (Integer integer : list) {
//			System.out.println(integer);
//		}
		System.out.println("-------------");
		list.add(1, 1983);
		System.out.println(list.getClass());
		for (int j = 0; j < list.size(); j++) {
			System.out.println(list.get(j));
		}
		System.out.println("size " + list.size());
		list.remove(2);
		for (int j = 0; j < list.size(); j++) {
			System.out.println(list.get(j));
		}
		System.out.println("size " + list.size());

	}

	public static void linkedList() {
		// Array vaf Linked is different
		List<Integer> list = new LinkedList<Integer>();
		list.add(10);// like[i] = 10
		list.add(510);// like[i] = 10

		list.add(50);// like[i] = 10

		list.add(5);// like[i] = 10

		// method 1:
		for (Integer item : list) { // Collection use for-each element
			System.out.println(item);
			// without index of array
		}
		// method 2
		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i));
			// with index of array
		}
	}

	public static void arraylist() {
		// <ClassType> only
		List<Integer> list = new ArrayList<Integer>();
		list.add(510);// like[i] = 10
		list.add(510);// like[i] = 10

		list.add(5);// like[i] = 10

		list.add(5);// like[i] = 10

		// method 1:
		for (Integer item : list) { // Collection use for-each element
			System.out.println(item);
			// without index of array
		}
		// method 2
		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i));
			// with index of array
		}

	}

}
