package com.quocbao.java.generic;

public class MainGeneric {
	// inner class macde in class {}
	static class Apple {
		public void print() {
			System.out.println("hi apple");
		}

	}

	static class Orange {
		public void print() {
			System.out.println("hi Orange");
		}
	}

	static class Box<T> {
		// has Apple or Orange, T it meams type
		// Type field, Type parameter , return type
		private T fruit;

		public T get() {
			// TODO Auto-generated method stub
			return null;
		}

//		public T getFruit() {
////			fruit = new T();// unknown type T
//			return fruit;
//		}
//
//		public void setFruit(T fruit) {
//			this.fruit = fruit;
//		}

	}

	public static void main(String[] args) {

		// Generic change Type when create instance

		Box apple = new Box<Apple>();
		apple.get();
		// Apple apple = new Apple();
		// apple.print();
		// Orange orange = new Orange();
		// orange.print();
	}
}
