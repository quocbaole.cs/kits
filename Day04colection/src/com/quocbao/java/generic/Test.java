package com.quocbao.java.generic;

public class Test {
	
	static class Apple {
		public void print() {
			System.out.println("apple");
		}
	}

	class Orange {
		public void print() {
			System.out.println("orange");
		}
	}

	static class Box<T> {
		private T t;

		public void add(T t) {
			this.t = t;
		}

		public T get() {
			return t;
		}
	}

	public static void main(String[] args) {
		Box<Apple> apple = new Box<Apple>();
		System.out.println(apple.get());
	}

}
