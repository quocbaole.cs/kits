package com.quocbao.java.oop.car;

import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		// data' Collection framework of Java
		int[] array = new int[10]; // need fixed length, max length
		// dynamic structure can changed length
		List<Integer> list = new ArrayList<Integer>();

		array[0] = 5;
		array[10] = 5; //cant save value with >length
		list.add(10);
		System.out.println(array.length);// max numbers of element in array
		System.out.println(list.size()); // current numbers of element in list
	}

}
