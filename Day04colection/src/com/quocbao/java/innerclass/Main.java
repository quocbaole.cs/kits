package com.quocbao.java.innerclass;

public class Main {
	//inner class macde in class {}
	static class Apple {
		public void print() {
			System.out.println("hi apple");
		}

	}

	static class Orange {
		public void print() {
			System.out.println("hi Orange");
		}
	}

	class Box {
		//has Apple or Orange 
		private Apple apple;
		public Box() {
			apple = new Apple();
		}
		public void print() {
			apple.print();
		}
	}
	public static void main(String[] args) {
		Apple apple = new Apple();
		apple.print();
		Orange orange = new Orange();
		orange.print();
	}

}
