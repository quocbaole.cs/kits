package com.quocbao.java.oop;

public class Article {
	// field
	private long seq;
	private String title;
	private String wirterName;
	private String content;

	public long getSeq() {
		return seq;
	}

	public void setSeq(long seq) {
		this.seq = seq;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getWirterName() {
		return wirterName;
	}

	public void setWirterName(String wirterName) {
		this.wirterName = wirterName;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
