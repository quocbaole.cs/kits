package com.quocbao.java.oop;

import java.util.Scanner;

public class MainBloger {
	static Scanner sc;
	static Article[] list;
	static int seq;

	public static void main(String[] args) {

		sc = new Scanner(System.in);// import java.until.Scanner
		list = new Article[10];
		seq = 0;
		while (true) {
			System.out.println("cmd >");
			String cmd = sc.nextLine();
			if (cmd.equals("new")) { // Create
				registArticle();
			}
			if (cmd.equals("edit")) {// Update
				// edit need to edit data :
				// 1. select article to edit, use array, need index
				// 2. use function
				// title, writer, content can be duplicated need unique value dont dupliacated
				System.out.println("select article's index");
				cmd = sc.nextLine();
				int idx = Integer.parseInt(cmd);// how to search index type int
				if (idx < seq) {
					editArticle(idx); // idx index to edit
				} else {
					System.out.println("invalid index");
				}
			}
		}
	}

	private static void editArticle(int idx) {
		Article article = list[idx];
		System.out.println(article.hashCode());
		System.out.println(list[idx].hashCode());

		System.out.println("Title >");
		String title = sc.nextLine();
		if (!title.isEmpty()) {
			article.setTitle(title);
		}
		System.out.println("Writer >");
		String writer = sc.nextLine();
		if (!writer.isEmpty()) {
			article.setWirterName(writer);
		}
		System.out.println("COntent >");
		String content = sc.nextLine();
		if (!content.isEmpty()) {
			article.setContent(content);
		}

	}

	private static void registArticle() {
		// TODO Auto-generated method stub
		// data read from keyboard
		System.out.println("Title >");
		String title = sc.nextLine();
		System.out.println("Writer >");
		String writer = sc.nextLine();
		System.out.println("COntent >");
		String content = sc.nextLine();

		// save data in Artical instance
		Article article = new Article();
		article.setTitle(title);
		article.setWirterName(writer);
		article.setContent(content);

		// instance name: artice
		System.out.println(article);

		// article fields
		if (seq < list.length) {
			list[seq] = article; // need index of array
			++seq;
			// ArrayIndexOutOfBounds
		}
	}

}
