package com.quocbao.java.string;

public class Test {

	public static void main(String[] args) {
//		useString01();
		// StringObject.useString01(); fixed error light

//		useString02();

		// addition String, use StringBuilder
//		useString03();
		// useString04();

		// String immutable ? nhung thu khong thay doi (cant change)
		String s1 = "number";
		System.out.println(s1);
//		String s2 = s1.replace('n', 'N'); //'n'->'N', change the same Create 
		// new String('N'number); referent s1 different s2
		s1 = changeN(s1);
		System.out.println(s1);

	}

	private static String changeN(String s1) {

		// TODO Auto-generated method stub
		return s1.replace('n', 'N');

	}

	private static void useString04() {
		// TODO Auto-generated method stub
		String s1 = "vietnam 2020 gdp";
		String s2 = "viet";
		System.out.println(s1.equals(s2));
		System.out.println(s2.equals(s1));// compare s1 and s2 have the same

		// s2 included in s1
		System.out.println(s1.contains(s2));
		// s1 included in s2
		System.out.println(s2.contains(s1));

		// s2 start in s1
		System.out.println(s1.startsWith(s2));
	}

//Note ://cong truc tiep chiem memrory 
//Khac phuc cong dai: StringBuilder 
	private static void useString03() {
		// TODO Auto-generated method stub

		StringBuilder builder = new StringBuilder("[");
		builder.append("name:");
		builder.append("jang,");
		builder.append("phone:");
		builder.append(110);
		System.out.println(builder.toString());

		//
		String s1 = "james";
		s1 = s1 + " dean";
		s1 += "-1010-2020";
		System.out.println(s1);
	}

	private static void useString02() {
		// TODO Auto-generated method stub
		String s1 = "james,1010";
		String[] splited = s1.split(",");
		System.out.println(splited.length);
		System.out.println(splited[0]);
		System.out.println(splited[1]);

		String s2 = String.join(" ", splited);
		// The static method join(CharSequence, CharSequence...) from the type String
		// should be accessed in a static way
		// ClassName.StaticMethod();
		System.out.println(s2);

	}

	private static void useString01() {
		// TODO Auto-generated method stub
		String s1 = "james";
//		String s2 = "james";
		String s2 = new String("james");
		String s3 = s2;

		// 1. reference same, value same
		// 2. reference difference, value can be same, or difference value
		//
		System.out.println(s3 == s2); // reference (compare value String but different address in memory)
		System.out.println(s3.equals(s2));// char value (address in memmory the same)

		System.out.println(s1 == s2); // reference (compare value String but different address in memory)
		System.out.println(s1.equals(s2));// char value (address in memmory the same)

	}

}