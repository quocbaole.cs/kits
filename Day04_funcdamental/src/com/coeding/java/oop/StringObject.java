package com.coeding.java.oop;

public class StringObject {

	public static void main(String[] args) {
		// String immutable ? 바뀌지 않는다.
		String s1 = "number";
		System.out.println(s1);
		s1 = changeN(s1);
		System.out.println(s1);
	}
	private static String changeN(String s1) {
		return s1.replace('n', 'N');// 'n'->'N'
	}
	private static void useString04() {
		String s1 = "vietnam 2020 gdp";
		String s2 = "viet";
		System.out.println( s1.equals(s2));
		System.out.println( s2.equals(s1));
		
		// s2 included in s1
		System.out.println(s1.contains(s2));
		// s1 included in s2
		System.out.println(s2.contains(s1));
		
		System.out.println(s1.startsWith(s2));
		
		
	}
	private static void useString03() {
		StringBuilder builder = new StringBuilder("[");
		builder.append("name:");
		builder.append("jang,");
		builder.append("phone:");
		builder.append(110);
		System.out.println(builder.toString());
		
		String s1 = "james";
		s1 = s1 + " dean";
		s1 += "-1010-2020";
		System.out.println(s1);
		
	}
	private static void useString02() {
		String s1 = "james/1010";
		String[] splited = s1.split("/");
		System.out.println(splited.length);
		System.out.println(splited[0]);
		System.out.println(splited[1]);
		String s2 = String.join(",", splited);
		// ClassName.StaticMethod();
		System.out.println(s2);
		
	}
	private static void useString01() {
		String s1 = "james";
		String s2 = new String("james");
		String s3 = s2;// 
		// 1. reference same , value same : 1 cay
		System.out.println( s3 == s2 );// reference
		System.out.println( s3.equals(s2) );// data value
		// 2. reference different, value can be same, different
		//		2 cay over		
		System.out.println( s1 == s2 );// reference
		System.out.println( s1.equals(s2) );// char value
		
	}

}
