package com.coeding.java.oop;

/** OOP core concepts
 * 	1. Inheritance
 * 	2. Abstract
 * 	3. Interface
 * 
 * 	Friday : team project exam
 * 
 * @author Administrator
 *
 */
public class ConsoleStarter {

	public static void main(String[] args) {
		// Sep's order present TV operation
		// few edit code when changed Type
		// ê¸°ì¡´ì�˜ í�´ëž˜ìŠ¤(class)ì�˜ ì½”ë“œ(code)ë¥¼ ìƒˆ í�´ëž˜ìŠ¤ë¥¼ ë§Œë“¤ ë•Œ ì‚¬ìš©í•  ìˆ˜ ìžˆê²Œ ë§Œë“ ë‹¤.
		TV tv1 = new TV();
		tv1.powerOn();
		tv1.volumnUp();
		tv1.volumnDown();
		tv1.powerOff();
		TV tv2 = new VinTV();// VinTV be used as TV
		tv2.powerOn();// overrided method
		tv2.volumnUp();
		tv2.volumnDown();
		tv2.powerOff();
//		tv2.internet();//<-- have no in TV class
		
		TV tv3 = new OppoTV();
		tv3.powerOn();
		tv3.volumnUp();
		tv3.volumnDown();
		tv3.powerOff();
		
	}

	

}
