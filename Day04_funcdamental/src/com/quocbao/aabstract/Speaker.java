package com.quocbao.aabstract;

//has abstract mehtod 
public abstract class Speaker{

	// method means to do

	// it not run content (abstract)
	/**
	 * developer A: soundDown
	 */
	public abstract void soundUp(); // un-implemented method

	public void soundDown() {
		// implemented method (it will run)
		System.out.println("down -- --");
		
	}

}
