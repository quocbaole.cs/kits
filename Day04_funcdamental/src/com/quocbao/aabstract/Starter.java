package com.quocbao.aabstract;

public class Starter {

	

	public static void main02(String[] args) {

		Printer printer = new InkjetPrinter();
		printer.connect();// by USB port
		printer.print();
		Printer printerlaser = new LaserPrinter();
		printerlaser.connect();// by USB port
		printerlaser.print();

		// Inhertance and Abstact, what different?
	}

	public static void main01(String[] args) {

		// Abstract Class cant create instance
		Speaker speaker = new BangSpeaker();

		speaker.soundUp();
		speaker.soundDown();

	}

}
