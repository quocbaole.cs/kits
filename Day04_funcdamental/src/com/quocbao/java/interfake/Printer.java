package com.quocbao.java.interfake;

public abstract class Printer {

	// common function any printer
	public void connect() {
		System.out.println("USB cable connected");
	}

	public abstract void print();// how to print ? how method print?

}
