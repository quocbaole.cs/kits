package com.quocbao.java.interfake;


public class WalkableImpl implements Walkable {

	@Override
	public void walk() {
		// TODO Auto-generated method stub
		System.out.println("go walk");
	}
	
	// annotation @Override means already has 
	@Override
	public void stop() {
		System.out.println("stop walk");
	}
	

}
