package com.quocbao.java.interfake;

public class Main {

	public static void main(String[] args) {
		Robot robot = new Robot();
		robot.walk();
		robot.run();
		robot.stop();
		
		Robot robotlg = new LgRobot();
		robotlg.walk();
		robotlg.run();
		
		Printer robotp = new LgRobot();
		Walkable robo = new LgRobot();
		robo.stop();
		
	}

	public static void main03(String[] args) {
		Walkable robo = new WalkableImpl();
		System.out.println(Walkable.version);
	}
}
