package com.quocbao.java.interfake;

//interface not class, can create instance 
//all method be abstract
// to use, have to implement
//field must be final which can't changed
public interface Walkable {
	final String version = "0.0.1-SNAPSHOT";
	public void walk();

	public void stop();

}
