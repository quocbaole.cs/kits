package com.coeding.java;

import java.io.IOException;

public class Statement01 {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		/*
		 * for(int i=2;i<=9;i++) { for(int j=1;j<=9;j++) { System.out.println(i + " * "
		 * + j + " = " +i*j); } }
		 */
		int i;
		//13 carrige return /r
		//10 line feed /n
		//<enter> 13 10
		while((i = System.in.read()) != -1)
		{
			System.out.println((char)i);
		}
		System.out.println("terminated");
			
		
	}

	private static void Loop() {
		int i=10;
		//for
		for (int j = 10; j > 0; j -= 2) {
			System.out.println(j);
		}

		System.out.println("\n");
		
		//while
		i=10;
		while (i > 0) {
			System.out.println(i);
			i -= 2;
		}

		System.out.println("\n");
		
		//do while
		do {
			i+=2;
			if(i>10) {
				break;
			}
			System.out.println(i);
		}while(i<=10);
	}
}
