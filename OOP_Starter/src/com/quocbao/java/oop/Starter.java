package com.quocbao.java.oop;

import java.util.Scanner;

/**
 * blog concepts - have article - writer blog CRUD - console : keyboard and
 * monitor
 * 
 * @author KIT1_009
 *
 */
public class Starter {
	
	static Scanner sc; // import java.until.Scanner
	static Articale[] list;
	static int seq;

	public static void main(String[] args) {
		// TODO: program entry point ( start)

		sc = new Scanner(System.in); // import java.until.Scanner
		list = new Articale[10];
		seq = 0;// index of list array, [0] start

		while (true) {
			System.out.println("cmd>");
			String cmd = sc.nextLine();
			if (cmd.equals("new")) {// Create
				registArticle();
			}
			if (cmd.equals("edit")) {// Update
				// edit need to edit data :
				// 1. select article to edit. use array , need index
				// 2.use function
				// title, writer, content can be duplicated need unique value dont duplicated
				System.out.println("select article's index >");
				cmd = sc.nextLine();
				int idx = Integer.parseInt(cmd); // how to search (select index) index's type int
				if (idx < seq) { // have article , if seq 3, [0][1][2]
					editArticle(idx); // idx index to edit
				} else {
					System.out.println("invaild index");
				}

			}
			if (cmd.equals("search")) {
				System.out.println("search writer>");
				cmd = sc.nextLine();
				Articale[] rs = searchArticleByWriter(cmd);// writer 1 : N article ?
				// writer can write many articles
			}

		}
	}

	private static Articale[] searchArticleByWriter(String cmd) {
		return null;
	}

	private static void editArticle(int idx) {
		Articale article = list[idx]; // get reference
		System.out.println("Title >" + article.getTitle());
		String title = sc.nextLine();
		// press only <enter>, empty string
		if (title.length() > 1) {
			// have input data
			article.setTitle(title);// over write data
		}
		System.out.println("Writer > " + article.getWriterName());
		String name = sc.nextLine();
		if (name.length() > 1) {
			// have input data
			article.setWriterName(name);
		}

		System.out.println("Content >" + article.getContent());
		String content = sc.nextLine();
		if (content.length() > 1) {// !content.isEmpty()
			article.setContent(content);
		}
//QUIZZ tai sao khong co code 
//		list[idx] = article; //already set, get 

	}

	private static void registArticle() {
		// TODO Auto-generated method stub
		// data read from keyboard
		System.out.println("Title >");
		String title = sc.nextLine();
		System.out.println("Writer > ");
		String name = sc.nextLine();
		System.out.println("Content >");
		String content = sc.nextLine();

		// save data in Article instance
		Articale article = new Articale();
		article.setTitle(title);
		article.setWriterName(name);
		article.setContent(content);

		// instance name: article
		System.out.println(article);

		// article fields
		System.out.println(article.getTitle());
		System.out.println(article.getWriterName());
		System.out.println(article.getContent());
		if (seq < list.length) {
			list[seq] = article;// need index of array
			++seq;
			// ArrayIndexOutOfBounds

		}
	}

}
