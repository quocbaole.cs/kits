package com.quocbao.java.oop.car;
/**
 * 
 * @author KIT1_009
 *
 */

//define Rule for our project 
//Method's parameter, return type
//interface is Rule of method

public interface Movable {

	public void go();

	public void back();

	public void turnLeft(double degree);

	public void turnRight(double degree);
}
