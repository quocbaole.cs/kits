package com.quocbao.java.oop.car;

//follow fule of Movable 
public abstract class Car implements Movable {
	// all Car has Tire
	private Tire[] front;// banh truoc [0]left, [1]right
	private Tire[] rear; // banh sau

	public Car() {
		front = new Tire[2];// assemble tire
		rear = new Tire[2];
		System.out.println("Car has Tire 404");
	}

	@Override
	public void go() {
		System.out.println("move fordward");

	}

	@Override
	public void back() {
		System.out.println("move backward");

	}

}
