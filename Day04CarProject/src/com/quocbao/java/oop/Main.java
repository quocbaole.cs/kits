package com.quocbao.java.oop;

import com.quocbao.java.oop.car.Movable;
import com.quocbao.java.oop.car.VinfastCar;

public class Main {
	public static void main(String[] args) {
		//Car is abstract clss 
		Movable car = new VinfastCar();
		//use car
		car.go();
		car.turnLeft(56);
		car.back();
		car.turnRight(56);
	}

}
