package com.quocbao.type.java;
/**
 *  my class 
 *  has what : fields 
 *  have to do : method
 *  -> create class 
 * @author KIT1_009
 *
 */
public class TV {

	private OppoSpeaker speaker; // "has" means fields 
	
	public TV() {
		speaker = new OppoSpeaker();
		System.out.println("TV instance");
	}
	
	
	
	public OppoSpeaker getSpeaker() {
		return speaker;
	}



	public void setSpeaker(OppoSpeaker speaker) {
		this.speaker = speaker;
	}



	// method, this object can to do
	public void powerOn() {
		System.out.println("TV on");
	}

	public void powerOff() {
		System.out.println("TV off");
	}
	public void volumnUp() {
		//uy quyen: Delegation 
		speaker.soundUp();
	}
	public void volumnDown() {
		speaker.soundDown();
	}

}
