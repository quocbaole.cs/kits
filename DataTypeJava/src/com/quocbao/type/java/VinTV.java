package com.quocbao.type.java;

//inheritance
//easy change Type. legacy code re-use (important)
//TV class field, method can copied and use 
//add field, method need 
public class VinTV extends TV {

	// i want to change method of Parent TV

	@Override
	public void powerOn() {
		System.out.println("TV on ");
		internet();
	}

	@Override
	public void volumnUp() {
		// if i wnat use speaker of parent 
		// 1. protected 
		// 2. getSpeaker in parent 
		
		getSpeaker().soundUp();
	//if not use speacker of parent 
		//make my speacker
	}

	public void internet() {
		System.out.println("using interent");
		getSpeaker().soundUp(); // child can't use private field of parent
	}

}
