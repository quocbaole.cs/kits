package com.coeding.java.oop;

/**
 * 
 * has info about book
 * 
 * 	make getter, setter
		Phát Đạt Nguyễn오후 4:02
		tính đóng gói
		encapsulation
 * @author Administrator
 *
 */
public class Book {
	// Access Modifier : private public protected (default)
	// field have to be private
	// method public
	private String title;
	private String writer;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getWriter() {
		return writer;
	}
	public void setWriter(String writer) {
		this.writer = writer;
	}

	// alt+shift+s > generate Getter/Setter
	
}




