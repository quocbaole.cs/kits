package com.coeding.java.oop;

import java.util.ArrayList;// Class
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;// Interface

import com.coeding.java.oop.car.Car;
import com.coeding.java.oop.car.VinfastCar;
// dont use *

/** OOP core concepts
 * 	1. Inheritance
 * 	2. Abstract
 * 	3. Interface
 * 
 * 	Friday : team project exam
 * 
 * 	inheritance, abstract, interface and generic, 
 * 	List
 * @author Administrator
 *
 */
public class ConsoleStarter {
	// inner class made in class { }
	static class Apple{
		public void print() {
			System.out.println("Hi apple");
		}
	}
	static class Orange{
		public void print() {
			System.out.println("Hi orange");
		}		
	}
	static class Box<T>{
		// only Type field, parameter, return
		// T is unknown type
		private T fruit;
		public void print() {
			System.out.println(fruit);
		}		
		public void set(T arg) {
			fruit = arg;
		}		
		public T get() {
			return fruit;
		}
	}
	public static void main(String[] args) {
		// Generic change Type when create instance
		// use abstract class, interface
		Box<String> box = new Box<String>();
		box.set(new String("adlfp"));
		box.print();
		
	}
	
	public static void main10(String[] args) {
		ArrayList<String> list = new ArrayList<String>();
		list.add("toy");
		list.add("apple");
		list.add("banana");
		list.add("banana");
		list.add("pineapple");
		System.out.println(list.size());
		for(String el : list) {
//			[0]toy [1]apple [2]banana<- [3]empty so stop
			if( el.equals("banana")) {
				list.remove(el);// my recommended dont remove in loop
			}
		}
		System.out.println(list.size());
		for(String el : list) {
			System.out.println(el);
		}
		
	}
	
	public static void main09(String[] args) {
		ArrayList<String> list = new ArrayList<String>();
		list.add("toy");//[0]
		list.add("apple");//[1]
		list.add("banna");//[2]
		list.add(1, "index1-Vaue");// means insert at [1]
		System.out.println(list.size());
//		list.remove(1);// index
		System.out.println(list.size());
//		Iterator<String> itr = list.iterator();
//		while( itr.hasNext() ) {// has element to get ?
//			String el = itr.next();// get element
//			System.out.println(el);
//		}
		String del = null;
		for(String el : list) {// for-each need has Iterator
			System.out.println(el);
			if(el.equals("apple")) {
				del = el;
//				list.remove(del);
				break;
			}
		}
		// after search
		if( del != null ) {// recommended
			list.remove(del);
		}
		// after remove
		for(String el : list) {// for-each need has Iterator
			System.out.println(el);
		}
		
	}
	
	public static void main08(String[] args) {
		// Array va Linked internal structure is different
		// but, usage is same (interface List )
		List<Integer> list = new LinkedList<Integer>();
		list.add(10);
		list.add(30);
		list.add(20);
		list.add(30);// 같은 값을 여러 번 추가할 수 있다
		for(Integer el : list) {
			System.out.println(el);
			// without index of array
		}
		for(int i=0; i<list.size(); i++) {
//			[0]10 [1]30 [2]20
			System.out.println( list.get(i) );// like list[i]
			// with index of array
		}
	}

	public static void main07(String[] args) {
		// <ClassType> only
		List<Integer> list = new ArrayList<Integer>();
		list.add(10);// like list[i] = 10;
		list.add(30);
		list.add(20);
		for(Integer el : list) {// Collection use for-each element
			System.out.println(el);
			// without index of array
		}
		for(int i=0; i<list.size(); i++) {
//			[0]10 [1]30 [2]20
			System.out.println( list.get(i) );// like list[i]
			// with index of array
		}
	}
	
	public static void main06(String[] args) {
		// 여러 개의 data 들을 다루는 방법 : Collection framework of Java
		int[] array = new int[5]; // need fixed max length
		
		// dynamic structure can changed length
		List<Integer> list = new ArrayList<Integer>();
		
		array[0] = 10;// add value in [0]
		list.add(10); // add value
		System.out.println(array.length);// max numbers of element in array
		System.out.println(list.size());// current numbers of element in list
	}

	public static void main05(String[] args) {
		// Car is abstract class
		Car car = new VinfastCar();// buy VinfastCar
		// use Car
		car.go();
		car.turnLeft(45.0);
		car.back();
		car.turnRight(90.0);
	}
	
	public static void main04(String[] args) {
		Walkable robot = new LgRobot();
		// 하나의 intance 를 다양한 type 사용할 수 있다.
		// 다형성 - Polymophism
	}
	
	public static void main03(String[] args) {
		Walkable robo = new WalkableImpl();
		System.out.println(Walkable.version);
	}
	
	public static void main02(String[] args) {
		Printer printer = new LaserPrinter();
		printer.connect();// by USB port
		printer.print();
	}
	
	public static void main01(String[] args) {
		// Sep's order present TV operation
		// few edit code when changed Type
		// 기존의 클래스(class)의 코드(code)를 새 클래스를 만들 때 사용할 수 있게 만든다.
		TV tv1 = new TV();
		tv1.powerOn();
		tv1.volumnUp();
		tv1.volumnDown();
		tv1.powerOff();
		TV tv2 = new VinTV();// VinTV be used as TV
		tv2.powerOn();// overrided method
		tv2.volumnUp();
		tv2.volumnDown();
		tv2.powerOff();
//		tv2.internet();//<-- have no in TV class
		
		TV tv3 = new OppoTV();
		tv3.powerOn();
		tv3.volumnUp();
		tv3.volumnDown();
		tv3.powerOff();
		
	}

	private static void assignType() {
		char letter = 'A';// value
		int num = 30;// integral value
		int[] list = new int[10];
		list[0] = 10;
		
		String name = "string value";
//		Array array = new Array();// create instance of Class
		
		String[] array = new String[10];// create instance of Array 
		System.out.println(array.getClass().getName());
		// element's Class type(Reference)
		array[0] = new String("first element");// create instance as element
		
	}

	private static void aboutType() {
		// primitive type startwith low case
		char letter;
		int num;
		long number;
		double lf;
		boolean state;
		// can't use .(dot) operator
		
		// Class type startwith upper case
		//	== Reference type : DataType@Address. go to
		//	API : already 
		
	}

}
