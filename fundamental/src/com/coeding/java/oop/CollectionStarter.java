package com.coeding.java.oop;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class CollectionStarter {

	public static void main(String[] args) {
//		list();
//		int hv = System.identityHashCode("william");
//		System.out.println( hv );
//		String s = new String("william");// ref is different
//		hv = System.identityHashCode(s);
//		System.out.println( hv );		
//		set();	// File, DB
//		map();	// include Set
	}

	/**
	 * Key-Value : like variables name = value;
	 * point !!
	 * 	variable's name cant be change in running
	 * 	k-v can be change and add in running
	 */
	private static void map() {
//		Map<K,V> is interface HashMap, TreeMap
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("a", 10);// add
		map.put("jang", 100);
		map.put("jang", 60);
		map.put("jang", 40);

		// get by Key(name)
		System.out.println( map.get("a") );
		System.out.println( map.get("jang") );
		
		// loop
		// 1. what need to get value in map
		Set<String> keys = map.keySet();
		for(String key  : keys) {
			System.out.println(map.get(key));
		}
	}

	private static void set() {
		TreeSet<Integer> set = new TreeSet<Integer>();
		// sorted by tree structure
		// want get according value
		set.add(10);
		set.add(90);
		set.add(20);
		set.add(30);
		set.add(2);
		System.out.println(set.first());// min
		System.out.println(set.last());// max
		if( set != null) {
			for(Integer el : set) {
				System.out.println(el);
			}
		}
		
	}

	/**
	 * hash value : value to 식별(identified)
	 * - "jang" -> 수학공식 -> 2345436
	 * - hash value is different, then data is different
	 * - data is same, hash value is same
	 */
	private static void hashset() {
//		Set<E> is interface can't save same value;
		Set<String> set = new HashSet<>();// Object.hashCode();
		// add
		set.add("jang");	// 123
		set.add("mario");	// 345
		set.add("william");	//	567
		set.add(new String("william"));	//	567
		set.add("tan tutor");	// 789
		set.add("lien tutor");	// 293
		// print all elements
		// 1.	return element by index(i)
		//		non sequencial when add, so without index
		// 2.	return numbers of element
		System.out.println( set.size() );
		// for-each
		for(String el : set) {
			System.out.println(el);
		}
		// for without index
		for(Iterator<String> itr = set.iterator(); 
				itr.hasNext();) {
			String el = itr.next();
			System.out.println(el);
		}
	}

	/**
	 * Collection framework : Library of Java
	 * - data structure algorithm
	 * - data structure is many data save way
	 * - use Generic to select actually use data type
	 * -	can change data type when create instance 
	 * - dynamic structure
	 * -	can changed length ( numbers of element )
	 * -	add(+), remove(-)	but array[] fixed length
	 */
	private static void list() {
//		List<E> is interface has Generic <element type>
		// List<> is parent of ArrayList, LinkedList
		List<Integer> alist = new ArrayList<Integer>();// diamond operator <>
		// Integer is wrapper class for int
		LinkedList<String> llist = new LinkedList<String>();
		
		// usage same
		alist.add(10);
		alist.add(20);
		llist.add("jang");
		System.out.println( alist.size());// current numbers
		System.out.println( llist.size());// but, array.length max numbers
		// loop : for(), for-each(), while()
		for(int i=0; i<alist.size();i++) {
			System.out.println( alist.get(i) );// array use []
		}
		for(int i=0; i<llist.size();i++) {
			System.out.println( llist.get(i) );
		}
		
		// homwork: hom nay
		//	invoke all methods of List
	}

}
