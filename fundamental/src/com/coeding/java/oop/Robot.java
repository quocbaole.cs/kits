package com.coeding.java.oop;

// multi(다중)
public class Robot extends LaserPrinter implements Walkable, Runable {
	@Override
	public void walk() {
		System.out.println("robot go");
	}

	@Override
	public void stop() {
		System.out.println("robot stop");		
	}

	@Override
	public void run() {
		System.out.println("robot running");
	}

}
