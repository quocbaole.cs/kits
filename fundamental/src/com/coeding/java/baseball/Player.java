package com.coeding.java.baseball;

public class Player {
	protected int answer;

	// random gia tri co 3 chu so (cac chu so phan biet nhau)
	
	public  Player() {
		answer = 157;
	}
	public void ready() {
		answer = genRandom();
		//answer = 123;
	}

	public  int[] reply(int value) {
		int strike = 0;
		int ball = 0;
		int copy = answer;
		for (int p1 = 0; p1 < 3; p1++) {
			int v1 = copy % 10;
			copy /= 10;
			int opp = value;
			for (int p2 = 0; p2 < 3; p2++) {
				int v2 = opp % 10;
				opp /= 10;
				if (v1 == v2) {
					if (p1 == p2) {
						strike++;
					} else {
						ball++;
					}
				}
			}
		}
		System.out.println("strike: "+strike);
		System.out.println("ball: "+ball);
		return new int[] { strike, ball };
		
	}

	public final int genRandom() {
		boolean[] flag = new boolean[10];
		int number = 0;
		for (int i = 0; i < 3; ++i) {
			int r;
			do {
				r = (int) (Math.random() * 100) % 9 + 1;
			} while (flag[r]);
			number = (number * 10) + r;
			flag[r] = true;
		}
		return number;
	}

	public int call() {
		return genRandom();
	}

	public void predict(int strike, int ball) {
		System.out.println("ë‚˜ëŠ” ì•„ë¬´ ìƒ�ê°�ì�´ ì—†ë‹¤ :" + answer);
	}

}
