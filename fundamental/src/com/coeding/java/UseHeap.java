package com.coeding.java;

public class UseHeap {

	public static void main(String[] args) {
		// Reference type : Class's object(instance)
		//	1. null means have no in memory
		//	2. void means have no return value;
		String jang = "Jang";// once
		System.out.println(jang + "@" + ((Object)jang).hashCode());
		System.out.println(jang + "@" + jang.hashCode());
		String[] names = new String[5];
		addName(names);
		System.out.println(names[0].hashCode());
		// String's hashCode is not memory address
		// Object's hashCode is memory address
		
	}
	
	private static void addName(String[] names) {
		names[0] = new String("Jang");		
	}

	private static void todo(String[] names) {
		names = new String[3];// in Heap
		// VM's GC
		System.out.println(names);
		// remeber it, locally new array
		names = null;// unused array, then remove it 
		// efficiently use memory
	}

}
