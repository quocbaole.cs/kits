package com.coeding.java.blog;

import java.util.Scanner;

/**
 * 
 * blog concepts
 * - have articles
 * - writer
 * blog CRUD
 * - console : keyboard and monitor
 * 
 * API(Application Programming Interfaces)
 * 
 * @author Administrator
 *
 */
public class Starter {
	static Scanner sc;
	static ArticleDAO dao;	
	// Control role
	public static void main(String[] args) {
		// TODO: program entry point ( start )
		sc = new Scanner( System.in );
		dao = new ArticleDAOImpl();
		while( true ) {
			Article[] list = dao.select();
			String view = showList(list);
			System.out.println(view);
			
			// request from keyboard
			System.out.println(">");
			String cmd = sc.nextLine();
			
			// control for request		
			if( cmd.equals("new")) {
				registArticle();
			}
			if( cmd.equals("edit")) {
				int idx = Integer.parseInt(cmd);//1
				editArticle(idx);
			}
			if( cmd.equals("search")) {
				Article vo = new Article();
				Article[] rs = dao.selectBy(vo);
				if( rs != null ) {
					String s = showList(rs);
					System.out.println(s);
				}
			}			
			
		}// end while
	}

	private static String showList(Article[] rs) {
		// rendering by View role
		String s = "search result\n";
		for(int i=0; i<rs.length;i++) {
			if( rs[i] != null ) {
				s += rs[i].getTitle();
				s += "("+rs[i].getWriterName()+")\n";
			}
		}
		s += "--------";
		return s;
	}

	private static void editArticle(int idx) {
		Article article = dao.selectOne(""+idx);// index of array 
		if( article == null ) {// have to check null, NPE
			System.out.println("invalid index");
			return;
		}
		System.out.println("Title > "+ article.getTitle());
		String title = sc.nextLine();
		// press only <enter> , empty string
		if( title.length() > 0) {			// have input data
			article.setTitle(title);// over write data
		}
		System.out.println("Writer > "+ article.getWriterName());
		String name = sc.nextLine();
		if( !name.isEmpty()) {			// have input data
			article.setWriterName(name);// over write data
		}
		System.out.println("Content > "+ article.getContent());
		String content = sc.nextLine();
		if( content.length() > 0) {			// have input data
			article.setContent(content);
		}
		// have to update in dao
		dao.update(article);
	}

	private static void registArticle() {
		// data read from keyboard
		System.out.println("Title > ");
		String title = sc.nextLine();
		System.out.println("Writer > ");
		String name = sc.nextLine();
		System.out.println("Content > ");
		String content = sc.nextLine();
		// save data in Article instance
		Article article = new Article();
		article.setTitle(title);
		article.setWriterName(name);
		article.setContent(content);
		// save to array in DAO
		// delegate to dao <-- call method of dao
		dao.insert(article);
	}

}
